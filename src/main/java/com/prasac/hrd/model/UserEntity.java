package com.prasac.hrd.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 11:18 AM
 * Email    : lina.sok@prasac.com.kh
 */
@Data
@NoArgsConstructor
public class UserEntity {

    private Long id;
    private String name;
    private String gender;

    public UserEntity(Long id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }
}
