package com.prasac.hrd.controller;

import com.prasac.hrd.model.UserEntity;
import com.prasac.hrd.service.UserService;
import com.prasac.hrd.util.ApiListResponse;
import com.prasac.hrd.util.ResponseFactory;
import com.prasac.hrd.util.ResponseModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 11:44 AM
 * Email    : lina.sok@prasac.com.kh
 */
@Controller
@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("user")
    public ResponseEntity<ApiListResponse<UserEntity>> getListUser(){

        ApiListResponse<UserEntity> apiListResponse ;
        try {
            apiListResponse = new ApiListResponse<>(HttpStatus.OK.getReasonPhrase(), true, HttpStatus.OK.value(), userService.getAllUser());
        }catch (Exception e){
            apiListResponse = new ApiListResponse<>(e.getMessage(), true, 404, null);
        }
        return new ResponseEntity<>(apiListResponse, HttpStatus.OK);
    }

    @GetMapping("user/{userId}")
    public ResponseModel<UserEntity> getUserService(@PathVariable Long userId) {
        try {
            return ResponseFactory.buildModel(userService.findUserById(userId));
        }catch (Exception e){
            return ResponseFactory.errorModel();
        }
    }

}
