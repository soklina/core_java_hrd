package com.prasac.hrd.util;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 1:27 PM
 * Email    : lina.sok@prasac.com.kh
 */
public class JResponseEntity<T>  {

    @JsonProperty("message")
    private String message;

    @JsonProperty("code")
    private int code;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("data")
    private List<T> data = new ArrayList<T>();

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public void addBody(T body) {
        if (body != null) {
            if (body instanceof List)
                data.addAll((Collection<? extends T>) body);
            else
                data.add(body);
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}

