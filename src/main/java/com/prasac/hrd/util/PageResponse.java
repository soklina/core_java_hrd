package com.prasac.hrd.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.scene.control.Pagination;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 1:28 PM
 * Email    : lina.sok@prasac.com.kh
 */
public class PageResponse<T> extends JResponseEntity {

    @JsonProperty("pagination")
    private Pagination page;

    public Pagination getPage() {
        return page;
    }

    public void setPage(Pagination page) {
        this.page = page;
    }
}

