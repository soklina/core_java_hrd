package com.prasac.hrd.util;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 12:01 PM
 * Email    : lina.sok@prasac.com.kh
 */
public abstract class ApiBaseResponse {

    private String message;
    private boolean status;
    private int code;

    public ApiBaseResponse(String message, boolean status, int code) {
        this.message = message;
        this.status = status;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
