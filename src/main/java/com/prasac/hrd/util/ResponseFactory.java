package com.prasac.hrd.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.scene.control.Pagination;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 1:26 PM
 * Email    : lina.sok@prasac.com.kh
 */
public class ResponseFactory {

    public static <T> JResponseEntity<T> build() {
        return new JResponseEntity<T>();
    }

    public static <T> ResponseModel<T> buildModel() {
        return new ResponseModel<T>();
    }

    public static <T> PageResponse<T> buildCustomPage(List<T> data, Pagination page) {
        PageResponse<T> pageResponse = new PageResponse();
        try {
            pageResponse.setPage(page);
            if (data != null && !data.isEmpty()) {
                pageResponse.setData(data);
                pageResponse.setCode(StatusMessage.OK.getStatusCode());
                pageResponse.setMessage(Message.SUCCESSED.getText());
                pageResponse.setStatus(true);
            } else {
                pageResponse.setMessage(Message.NO_DATA.getText());
                pageResponse.setCode(StatusMessage.NOT_FOUND.getStatusCode());
                pageResponse.setStatus(false);
                pageResponse.setData(new ArrayList<T>());
            }
        } catch (Exception e) {
            pageResponse.setMessage(Message.INTERNAL_SERVER_ERROR.getText());
            pageResponse.setCode(StatusMessage.INTERNAL_SERVER_ERROR.getStatusCode());
            pageResponse.setData(new ArrayList<T>());
            e.printStackTrace();
        }
        return pageResponse;
    }

    public static <T> JResponseEntity<T> build(List<T> data) {
        JResponseEntity<T> jResponseEntity = build();
        try {
            if (data != null && !data.isEmpty()) {
                jResponseEntity.setMessage(Message.SUCCESSED.getText());
                jResponseEntity.setCode(StatusMessage.OK.getStatusCode());
                jResponseEntity.setStatus(true);
                jResponseEntity.setData(data);

            } else {
                jResponseEntity.setMessage(Message.NO_DATA.getText());
                jResponseEntity.setCode(StatusMessage.NOT_FOUND.getStatusCode());
                jResponseEntity.setStatus(false);
                jResponseEntity.setData(new ArrayList<T>());
            }
        } catch (Exception e) {
            jResponseEntity.setMessage(Message.INTERNAL_SERVER_ERROR.getText());
            jResponseEntity.setCode(StatusMessage.INTERNAL_SERVER_ERROR.getStatusCode());
            jResponseEntity.setData(new ArrayList<T>());
            e.printStackTrace();
        }
        return jResponseEntity;
    }

    public static <T> ResponseModel<T> buildModel(T data) {
        ResponseModel<T> responseModel = buildModel();
        try {
            if (data != null) {
                responseModel.setMessage(Message.SUCCESSED.getText());
                responseModel.setCode(StatusMessage.OK.getStatusCode());
                responseModel.setStatus(true);
                responseModel.setData(data);
            } else {
                responseModel.setMessage(Message.NO_DATA.getText());
                responseModel.setCode(StatusMessage.NOT_FOUND.getStatusCode());
                responseModel.setStatus(false);
                responseModel.setData(null);
            }
        } catch (Exception e) {
            responseModel.setMessage(Message.INTERNAL_SERVER_ERROR.getText());
            responseModel.setCode(StatusMessage.INTERNAL_SERVER_ERROR.getStatusCode());
            responseModel.setData(null);
            e.printStackTrace();
        }
        return responseModel;
    }

    public static <T> ResponseModel<T> buildModel(String message, Integer code, T data) {
        ResponseModel<T> responseModel = buildModel();
        responseModel.setMessage(message);
        responseModel.setCode(code);
        responseModel.setStatus(true);
        if (Objects.nonNull(data))
            responseModel.setData(data);
        return responseModel;
    }

    public static <T> ResponseModel<T> errorModel() {
        ResponseModel<T> responseModel = buildModel();
        responseModel.setMessage(Message.INTERNAL_SERVER_ERROR.getText());
        responseModel.setCode(StatusMessage.INTERNAL_SERVER_ERROR.getStatusCode());
        responseModel.setStatus(false);
        responseModel.setData(null);
        return responseModel;
    }

    public static <T> JResponseEntity<T> created(List<T> data) {
        JResponseEntity<T> jResponseEntity = build();
        jResponseEntity.setMessage(Message.CREATED.getText());
        jResponseEntity.setCode(StatusMessage.CREATED.getStatusCode());
        jResponseEntity.setData(data);
        return jResponseEntity;
    }

    public static <T> JResponseEntity<T> error() {
        JResponseEntity<T> jResponseEntity = build();
        jResponseEntity.setMessage(Message.INTERNAL_SERVER_ERROR.getText());
        jResponseEntity.setCode(StatusMessage.INTERNAL_SERVER_ERROR.getStatusCode());
        jResponseEntity.setStatus(false);
        jResponseEntity.setData(new ArrayList<T>());
        return jResponseEntity;
    }

    public static <T> JResponseEntity<T> build(String message, Integer code, T body) {
        JResponseEntity<T> jResponseEntity = build();
        jResponseEntity.setMessage(message);
        jResponseEntity.setCode(code);
        jResponseEntity.setStatus(true);
        jResponseEntity.addBody(body);
        return jResponseEntity;
    }
}
