package com.prasac.hrd.util;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 1:23 PM
 * Email    : lina.sok@prasac.com.kh
 */
public class ResponseModel<T> {

    @JsonProperty("message")
    private String message;

    @JsonProperty("code")
    private int code;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("data")
    private T data;

    public ResponseModel() {

    }

    public ResponseModel(String message, int code, T data) {
        this.setMessage(message);
        this.setCode(code);
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
