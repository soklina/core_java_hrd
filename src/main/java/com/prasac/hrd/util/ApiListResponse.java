package com.prasac.hrd.util;

import java.util.List;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 12:00 PM
 * Email    : lina.sok@prasac.com.kh
 */
public class ApiListResponse<T> extends ApiBaseResponse {

    List<T> data;

    public ApiListResponse(String message, boolean status, int code, List<T> data) {
        super(message, status, code);
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public void setMessage(String message) {
        super.setMessage(message);
    }

    @Override
    public boolean isStatus() {
        return super.isStatus();
    }

    @Override
    public void setStatus(boolean status) {
        super.setStatus(status);
    }

    @Override
    public int getCode() {
        return super.getCode();
    }

    @Override
    public void setCode(int code) {
        super.setCode(code);
    }
}
