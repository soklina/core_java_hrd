package com.prasac.hrd.service;

import com.prasac.hrd.model.UserEntity;

import java.util.List;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 11:33 AM
 * Email    : lina.sok@prasac.com.kh
 */

public interface UserService {

    List<UserEntity> getAllUser();

    UserEntity findUserById(Long userId);

    void save(UserEntity userEntity);
}
