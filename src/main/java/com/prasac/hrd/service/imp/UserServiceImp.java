package com.prasac.hrd.service.imp;

import com.prasac.hrd.model.UserEntity;
import com.prasac.hrd.repositories.UserRepositories;
import com.prasac.hrd.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 11:34 AM
 * Email    : lina.sok@prasac.com.kh
 */
@Service
public class UserServiceImp implements UserService {

    private final UserRepositories userRepositories;

    public UserServiceImp(UserRepositories userRepositories) {
        this.userRepositories = userRepositories;
    }

    @Override
    public List<UserEntity> getAllUser() {
        return userRepositories.getUserList();
    }

    @Override
    public UserEntity findUserById(Long userId) {
        return userRepositories.findUserById(userId);
    }

    @Override
    public void save(UserEntity userEntity) {

    }
}
