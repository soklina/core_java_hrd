package com.prasac.hrd.repositories;

import com.prasac.hrd.model.UserEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sok Lina
 * Date     : 7/22/2020, 11:20 AM
 * Email    : lina.sok@prasac.com.kh
 */
@Repository
public class UserRepositories {

    List<UserEntity> userEntity = new ArrayList<>();
    private Object UserEntity;

    {
        userEntity.add(new UserEntity((long) 1, "lina", "male"));
        userEntity.add(new UserEntity((long) 2,"sok","male"));
        userEntity.add(new UserEntity((long) 3, "chhiv", "male"));
        userEntity.add(new UserEntity((long) 4,"si mun","male"));
    }

    public List<UserEntity> getUserList(){
        return this.userEntity;
    }

    public  UserEntity findUserById(Long userId){
        for (UserEntity user:userEntity) {
            if (user.getId().equals(userId)){
                return user;
            }
        }
        return null;
    }

}
